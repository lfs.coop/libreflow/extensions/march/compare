import re
from kabaret import flow
from libreflow.baseflow.file import GenericRunAction


class AnimKeyPathSessionValue(flow.values.SessionValue):

    _action = flow.Parent()
   
    def revert_to_default(self):
        value = self.root().project().get_action_value_store().get_action_value(
            self._action.name(),
            self.name(),
        )
        if value is None:
            default_values = {}
            default_values[self.name()] = self.get()

            self.root().project().get_action_value_store().ensure_default_values(
                self._action.name(),
                default_values
            )
            return self.revert_to_default()

        self.set(value)


class CompareWithAnimKeyAction(GenericRunAction):

    ICON = ('icons.libreflow', 'compare-previews')

    _file = flow.Parent()
    _shot = flow.Parent(5)
    _anim_key_path = flow.SessionParam('', AnimKeyPathSessionValue).ui(hidden=True)

    def runner_name_and_tags(self):
        return 'RV', []
    
    def get_version(self, button):
        return None

    @classmethod
    def supported_extensions(cls):
        return ["mp4","mov"]

    def allow_context(self, context):
        self._anim_key_path.revert_to_default()
        file_name = self._anim_key_path.get().split('/')[1] if self._anim_key_path.get() != '' else ''
        return (
            context 
            and self._file.format.get() in self.supported_extensions()
            and self._file.name() != file_name
        )

    def needs_dialog(self):
        if self._anim_key_path.get() != '':
            task_name, file_name = self._anim_key_path.get().split('/')
            self._anim_key_path.set(
                self._get_last_revision_path(task_name, file_name)
            )

        return (self._anim_key_path.get() == '')
    
    def get_buttons(self):
        if self._anim_key_path.get() == '':
            self.message.set(
                '''
                <h2>Can\'t find the AnimKey.</h2>\n
                Check if path parameter is correctly setted in Action Value Store.
                '''
            )
        
        return ['Close']
    
    def extra_argv(self):
        return [
            '-wipe', '-autoRetime', '0',
            '[', '-rs', '1', self._file.get_head_revision().get_path(), ']',
            '[', '-volume', '0', '-rs', '1', self._anim_key_path.get(), ']'
        ]

    def run(self, button):
        if button == 'Close':
            return
        
        return super(CompareWithAnimKeyAction, self).run(button)

    def _get_last_revision_path(self, task_name, file_name):
        path = ''

        if self._shot.tasks.has_mapped_name(task_name):
            task = self._shot.tasks[task_name]
            name, ext = file_name.rsplit('.', 1)

            if task.files.has_file(name, ext):
                f = task.files[f'{name}_{ext}']
                r = f.get_head_revision()

                if r is not None and r.get_sync_status() == 'Available':
                    path = r.get_path()

        return path


def compare_anim_key(parent):
    if re.match('^/march/films/[^/]+/sequences/[^/]+/shots/[^/]+/tasks/[^/]+/files/[^/]+_mov$', parent.oid()):
        r = flow.Child(CompareWithAnimKeyAction).ui(label='Compare with AnimKey')
        r.name = 'compare_anim_key'
        r.index = None
        return r


def install_extensions(session): 
    return {
        "march": [
            compare_anim_key,
        ],
    }
